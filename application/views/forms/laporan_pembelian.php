<table width="93%" border="0">
      <tr align="center" >
        <td><h3>
        Laporan Data Pembelian</br>
       Dari Tanggal 2019-02-01 s/d 2019-04-30</h3></td>
      
      </tr>
      
	<table width="100%" align="left">
    
  <td colspan="5"><a href="<?=base_url();?>Pembelian/report">BACK</a></td></br>
  
    <table width="100%" border="1" cellspacing="5">
      <tr align="center" bgcolor="#00FF00">
        <td>No</td>
        <td>Id Pembelian</td>
        <td>No Transaksi</td>
        <td>Tanggal</td>
        <td>Total Barang</td>
        <td>Qty</td>
        <td>Jumlah Nominal Pembelian</td>
        </td>
      </tr>
<?php
  		$no = 0;
      $total_keseluruhan = 0;

	  foreach ($data_pembelian as $data) {
		$no++;


    ?>
      <tr align="center">
        <td><?=$no;?></td>
        <td><?= $data->id_pembelian_h; ?></td>
        <td><?= $data->no_transaksi; ?></td>
        <td><?= $data->tanggal; ?></td>
        <td><?= $data->stok; ?></td>
        <td><?= $data->qty; ?></td>
        <td>Rp.<?=number_format($data->jumlah); ?></td>
        </tr>
      
    <?php
      $total_keseluruhan+= $data->jumlah;
    }
     ?>
  <tr align="center" bgcolor="green">	
   <td>Total Keseluruhan Pembelian</td> 
<td>Rp.<?=number_format($total_keseluruhan); ?></td>
    </table>
  </body> 
</html>