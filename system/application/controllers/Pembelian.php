<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("supplier_model");
		$this->load->model("pembelian_model");
		$this->load->model("barang_model");
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	{
		$this->listpembelian();
	}
	
	public function listpembelian()
	{
		$data['data_pembelian'] = $this->pembelian_model->tampilDataPembelian();
		$data['content']	   ='forms/Home_pembelian';
		$this->load->view('home_2', $data);

		
	}
	
	public function input_h()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		
		//if (!empty($_REQUEST)) {
			
			//$m_pembelian_h = $this->pembelian_model;
			//$m_pembelian_h->savePembelianHeader();
			//$id_terakhir = array();
			
			//panggil ID transaksi terakhir
			//$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();

			//redirect("pembelian/input_d/" . $id_terakhir, "refresh");$validation = $this->form_validation;
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules());
			
			if ($validation->run()){
				$this->pembelian_model->savePembelianHeader();
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("pembelian/index", "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/input_pembelian';
			$this->load->view('home_2', $data);
		
		//$this->load->view('input_pembelian', $data);
	}
	
	public function input_d($id_pembelian_header)
	{
		
		$data['id_header']				= $id_pembelian_header;
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
		
		//if (!empty($_REQUEST)) {
			
			//save detail
			//$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			//proses update stok
			//$kode_barang	= $this->input->post('kode_barang');
			//$qty			= $this->input->post('qty');
			//$this->barang_model->updateStok($kode_barang, $qty);
			
			//redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules1());
			
			if ($validation->run()){
				$this->pembelian_model->savePembelianDetail($id_pembelian_header);
				$this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		}
			
		//$this->load->view('input_karyawan', $data);
		$data['content'] = 'forms/input_pembelian_d';
			$this->load->view('home_2', $data);
		
		
		//$this->load->view('input_pembelian_d', $data);
	}
	public function report()
	{
		$data['content'] 	= 'forms/datepicker';
		$this->load->view('home_2', $data);
}
	public function laporanPembelian()

	{
		$data['data_pembelian'] = $this->pembelian_model->tampilLaporanPembelian();
		$data['content']	= 'forms/laporan_pembelian';
		$this->load->view('home_2', $data);

	}
}



